import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth, onAuthStateChanged } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyAZ5s07XnCdRZpLDH8r6Qe2YEqjwf8lK3k",
  authDomain: "netweaves-e0ab2.firebaseapp.com",
  projectId: "netweaves-e0ab2",
  storageBucket: "netweaves-e0ab2.appspot.com",
  messagingSenderId: "954372764798",
  appId: "1:954372764798:web:50d01c8ad112ad9900942f"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);

export { auth, db, onAuthStateChanged };
